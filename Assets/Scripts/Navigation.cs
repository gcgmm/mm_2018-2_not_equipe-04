﻿using System.Collections.Generic;

public class Navigation
{
    public string Scene { get; private set; }
    public Dictionary<string, string> Parameters { get; private set; }

    public Navigation(string scene, Dictionary<string, string> parameters)
    {
        Scene = scene;
        if (parameters == null) Parameters = new Dictionary<string, string>();
        else Parameters = parameters;
    }
}
