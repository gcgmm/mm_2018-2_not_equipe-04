﻿
public class DefaultKeys
{
    public const string BOTAO_B = "joystick button 1";
    public const string BOTAO_X = "joystick button 2";
    public const string BOTAO_A = "joystick button 0";
    public const string BOTAO_Y = "joystick button 3";
    public const string BOTAO_START = "joystick button 15";
}
