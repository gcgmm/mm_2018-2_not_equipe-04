﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TMPro;
using UnityEngine;
using UnityEngine.Video;

public class SelectionAnimals : MonoBehaviour
{
    public GameObject PainelVideo;
    public int Vidas;
    private VideoPlayer player;
    public VideoClip[] movies;

    public bool[] AnimalsAcertados;
    public bool[] LocaisAcertados;
    public int[] AnimalsToLocal;
    public TextMeshPro TextoVidas;

    public int PosicaoAtual = 0;
    public int QuantidadeAnimals = 0;
    public string[] names;
    public GameObject[] Animals;
    public Vector3[] OrderPositions;
    public TextMeshPro nameAnimal;

    public int PosicaoAtualRegiao = 0;
    public GameObject canvas;
    public TextMeshProUGUI[] labels;
    public string[] locais;

    public Material ShaderHud;
    public GameObject[] PlatesHud;

    private bool IniciouVideo = true;
    private bool EmMovimento = false;
    private bool Initializer = true;
    private DateTime timerEmMovimento = DateTime.Now;
    private Stopwatch timeoutEmMovimento;

    // Use this for initialization
    void Start()
    {
        BreadCrumb.Initialize("animals");
        if (BreadCrumb.GetParameters().ContainsKey("POSICAO"))
        {
            PosicaoAtual = Convert.ToInt32(BreadCrumb.GetParameters()["POSICAO"]);
        }
        Initializer = true;

        for(int i = 0; i < locais.Length && i < labels.Length; i++)
        {
            labels[i].SetText(locais[i]);
        }

        player = PainelVideo.GetComponent<VideoPlayer>();

        TextoVidas.SetText("Vidas: " + Vidas);
    }

    // Update is called once per frame
    void Update()
    {
        ControleInicializacao();

        if (player.isPlaying)
        {
            IniciouVideo = true;
        }
        else if (IniciouVideo)
        {
            if (PainelVideo.activeInHierarchy) ExitVideo();

            ClickButomB();
            ClickButomA();

            if (!canvas.activeInHierarchy)
            {
                ControlesAnimais();
            }
            else
            {
                ControlesLocais();
            }
        }
    }

    private void ClickButomA()
    {
        if (Input.GetKeyDown(DefaultKeys.BOTAO_A) ||
                        Input.GetKeyDown(KeyCode.R))
        {
            if (canvas.activeInHierarchy)
            {
                if (AnimalsToLocal[PosicaoAtual] == PosicaoAtualRegiao)
                    AcertouRelacaoAnimalToLocal();
                else ErrouRelacaoAnimalToLocal();
            }
            else
            {
                if (!AnimalsAcertados[PosicaoAtual])
                {
                    canvas.SetActive(true);
                }
            }
        }
    }

    private void ErrouRelacaoAnimalToLocal()
    {
        Vidas--;
        TextoVidas.SetText("Vidas: " + Vidas);
        if (Vidas == 0)
        {
            BreadCrumb.GoToScene("perdeu");
        }
    }

    private void AcertouRelacaoAnimalToLocal()
    {
        AnimalsAcertados[PosicaoAtual] = true;
        LocaisAcertados[PosicaoAtualRegiao] = true;

        //MOSTRAR VIDEO
        PainelVideo.SetActive(true);
        player.clip = movies[PosicaoAtual];
        player.Play();
        IniciouVideo = false;
        canvas.SetActive(false);
    }

    private void ClickButomB()
    {
        if (Input.GetKeyDown(DefaultKeys.BOTAO_B) ||
                        Input.GetKeyDown(KeyCode.Escape))
        {
            if (canvas.activeInHierarchy)
            {
                canvas.SetActive(false);
            }
            else
            {
                BreadCrumb.Return();
            }
        }
    }

    private void ExitVideo()
    {
        PainelVideo.SetActive(false);

        int qtdAcerto = 0;
        foreach (var acertou in AnimalsAcertados) if (acertou) qtdAcerto++;
        if (qtdAcerto == AnimalsAcertados.Length)
        {
            BreadCrumb.GoToScene("ganhou");
        }
        else
        {
            RefreshHud();
            NextLocal();
            AtualizarLocais();
        }
    }

    private void ControlesLocais()
    {
        float axis = Input.GetAxis("Vertical");
        bool axisIsLeft = axis <= -0.5;
        bool axisIsRigth = axis >= 0.5;
        if (!EmMovimento)
        {
            ControleMovimentacaoLocal(axisIsLeft, axisIsRigth);
        }

        PararMovimento(axisIsLeft, axisIsRigth);

    }

    private void ControleMovimentacaoLocal(bool axisIsTop, bool axisIsDown)
    {
        if (axisIsTop && PosicaoAtualRegiao < locais.Length)
        {
            NextLocal();
        }
        else if (axisIsDown && PosicaoAtualRegiao >= 0)
        {
            do
            {
                PosicaoAtualRegiao--;
                EmMovimento = true;
                if (PosicaoAtualRegiao < 0)
                {
                    PosicaoAtualRegiao = locais.Length - 1;
                }
            } while (LocaisAcertados[PosicaoAtualRegiao]);
        }

        if (EmMovimento)
        {
            timerEmMovimento = DateTime.Now;
            timeoutEmMovimento = Stopwatch.StartNew();
            AtualizarLocais();
        }
    }

    private void NextLocal()
    {
        do
        {
            PosicaoAtualRegiao++;
            EmMovimento = true;
            if (PosicaoAtualRegiao >= locais.Length)
            {
                PosicaoAtualRegiao = 0;
            }
        } while (LocaisAcertados[PosicaoAtualRegiao]);
    }

    private void AtualizarLocais()
    {
        for (int i = 0; i < labels.Length; i++)
        {
            labels[i].color = Color.white;

            if(i == PosicaoAtualRegiao)
            {
                labels[i].color = Color.blue;
            }

            if (LocaisAcertados[i])
            {
                labels[i].color = Color.green;
            }
        }
    }

    private void ControlesAnimais()
    {
        float axis = -Input.GetAxis("Horizontal");
        bool axisIsLeft = axis <= -0.5;
        bool axisIsRigth = axis >= 0.5;
        if (!EmMovimento)
        {
            ControleMovimentacao(axisIsLeft, axisIsRigth);
        }

        PararMovimento(axisIsLeft, axisIsRigth);

        RotacionarAnimal();
    }

    private void ControleMovimentacao(bool axisIsLeft, bool axisIsRigth)
    {
        if (axisIsLeft && PosicaoAtual < QuantidadeAnimals)
        {
            PosicaoAtual++;
            EmMovimento = true;
            if (PosicaoAtual >= QuantidadeAnimals)
            {
                PosicaoAtual = 0;
            }
        }
        else if (axisIsRigth && PosicaoAtual >= 0)
        {
            PosicaoAtual--;
            EmMovimento = true;
            if (PosicaoAtual < 0)
            {
                PosicaoAtual = QuantidadeAnimals - 1;
            }
        }

        if (EmMovimento)
        {
            timerEmMovimento = DateTime.Now;
            timeoutEmMovimento = Stopwatch.StartNew();
            AtualizarPosicoes();
            BreadCrumb.GetParameters()["POSICAO"] = PosicaoAtual.ToString();
        }
    }

    private void ControleInicializacao()
    {
        if (Initializer)
        {
            BreadCrumb.GetParameters()["POSICAO"] = PosicaoAtual.ToString();
            Initializer = false;
            AtualizarPosicoes();
        }
    }

    private void AtualizarPosicoes()
    {
        int animalControl = PosicaoAtual;
        for (int i = 0; i < QuantidadeAnimals; i++)
        {
            AtualizaPosicao(Animals[animalControl], OrderPositions[i]);

            animalControl++;
            if (animalControl >= QuantidadeAnimals)
            {
                animalControl = 0;
            }
        }
        nameAnimal.SetText(names[PosicaoAtual]);
        RefreshHud();
    }

    private void RefreshHud()
    {
        Color color = Color.white;
        if (AnimalsAcertados[PosicaoAtual])
        {
            color = Color.green;
        }
        foreach (var plateHud in PlatesHud)
        {
            Renderer renderer = plateHud.GetComponent<Renderer>();
            renderer.material.SetColor("_Color", color);
        }
    }

    private void PararMovimento(bool axisIsLeft, bool axisIsRigth)
    {
        if (!axisIsLeft && !axisIsRigth)
        {
            EmMovimento = false;
        }
        else
        {
            TimeOutMovimento();
        }
    }

    private void RotacionarAnimal()
    {
        float y = -Input.GetAxis("RHorizontal");
        float x = -Input.GetAxis("RVertical");
        if (x != 0 || y != 0)
        {
            Animals[PosicaoAtual].transform.localEulerAngles = new Vector3(
                                    Animals[PosicaoAtual].transform.localEulerAngles.x, // + x,
                                    Animals[PosicaoAtual].transform.localEulerAngles.y + y,
                                    Animals[PosicaoAtual].transform.localEulerAngles.z);
        }
    }

    private void TimeOutMovimento()
    {
        if (EmMovimento)
        {
            if (timeoutEmMovimento.ElapsedMilliseconds > 500)
            {
                EmMovimento = false;
            }
        }
    }

    private void AtualizaPosicao(GameObject animal, Vector3 position)
    {
        animal.transform.position = new Vector3(
            position.x,
            animal.transform.position.y,
            position.z);
    }
    
}