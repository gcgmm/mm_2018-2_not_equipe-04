﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Abertura : MonoBehaviour {

    public string scene;

    // Use this for initialization
    void Start ()
    {
        BreadCrumb.Initialize("Abertura");
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(DefaultKeys.BOTAO_A) ||
            Input.GetKeyDown(KeyCode.R))
        {
            BreadCrumb.GoToScene(scene);
        }
    }
}
