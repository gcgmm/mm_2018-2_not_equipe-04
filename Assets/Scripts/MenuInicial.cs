﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuInicial : MonoBehaviour {

	public Transform selecionado;
	public Transform iniciar;
	public Transform opcoes;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow)) {
			if (selecionado.position == iniciar.position) {
				selecionado.position = opcoes.position;
			} else {
				selecionado.position = iniciar.position;
			}
		}
	}
}
