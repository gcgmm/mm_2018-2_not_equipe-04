﻿using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class BreadCrumb
{
    private static BreadCrumb instance = null;
    private List<Navigation> scenes = new List<Navigation>();

    public static void Initialize(string scene)
    {
        if (GetBreadCrumb().scenes.Count <= 0)
        {
            GetBreadCrumb().scenes.Add(new Navigation(scene, new Dictionary<string, string>()));
        }
    }

    public static void GoToScene(string scene, Dictionary<string, string> parameters = null)
    {
        if (parameters == null) parameters = new Dictionary<string, string>();
        GetBreadCrumb().InnerGoToScene(scene, parameters);
    }

    public static void Return()
    {
        GetBreadCrumb().InnerReturn();
    }

    public static Dictionary<string, string> GetParameters()
    {
        return GetBreadCrumb().InnerGetParameters();
    }

    private BreadCrumb()
    {
    }

    private static BreadCrumb GetBreadCrumb()
    {
        if (instance == null)
        {
            instance = new BreadCrumb();
        }
        return instance;
    }

    private void InnerGoToScene(string scene, Dictionary<string, string> parameters)
    {
        SceneManager.LoadScene(scene);
        scenes.Add(new Navigation(scene, parameters));
    }

    private void InnerReturn()
    {
        int qtd = scenes.Count;
        if (qtd > 1)
        {
            scenes.RemoveAt(qtd - 1);
            SceneManager.LoadScene(scenes[qtd - 2].Scene);
        }
    }

    private Dictionary<string, string> InnerGetParameters()
    {
        return scenes[scenes.Count - 1].Parameters;
    }
}
