﻿using UnityEngine;

public class MenuPrincipal : MonoBehaviour
{

    public string sceneRight;
    public string sceneLeft;
    public bool rightSelected = true;

    public GameObject selector;
    public GameObject[] respawns;

    private bool changed = false;
    void Start()
    {
        if (respawns == null)
            respawns = GameObject.FindGameObjectsWithTag("selector");

        foreach (GameObject respawn in respawns)
        {
            Instantiate(selector, respawn.transform.position, respawn.transform.rotation);
        }

        if (BreadCrumb.GetParameters().ContainsKey("RIGHT"))
        {
            rightSelected = BreadCrumb.GetParameters()["RIGHT"] == "S";
            changed = true;
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(DefaultKeys.BOTAO_B) ||
            Input.GetKeyDown(KeyCode.Escape))
        {
            BreadCrumb.Return();
        }

        if (Input.GetKeyDown(DefaultKeys.BOTAO_A) ||
            Input.GetKeyDown(KeyCode.R))
        {
            if (rightSelected)
            {
                BreadCrumb.GoToScene(sceneRight);
            }
            else
            {
                BreadCrumb.GoToScene(sceneLeft);
            }
        }

        float lado = Input.GetAxis("Horizontal");
        if (lado == 1)
        {
            rightSelected = true;
            changed = true;
        }
        else if (lado == -1)
        {
            rightSelected = false;
            changed = true;
        }

        if (changed)
        {
            BreadCrumb.GetParameters()["RIGHT"] = rightSelected ? "S" : "N";
            if (rightSelected)
            {
                selector.transform.position = new Vector3(5, selector.transform.position.y, selector.transform.position.z);
                selector.transform.localEulerAngles = new Vector3(selector.transform.localEulerAngles.x, -90, selector.transform.localEulerAngles.z);
            }
            else
            {
                selector.transform.position = new Vector3(-5, selector.transform.position.y, selector.transform.position.z);
                selector.transform.localEulerAngles = new Vector3(selector.transform.localEulerAngles.x, 90, selector.transform.localEulerAngles.z);
            }
            changed = false;
        }
    }
}
