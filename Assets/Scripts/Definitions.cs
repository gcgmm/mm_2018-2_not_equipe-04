﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Definitions
{
    public static string[] Animals = { "Baleia", "Tubarão", "Urso", "Pinguin", "Aguia" };
    public static string[][] AnimalDescriptions =
    {
        new string[] { "Sou Grande", "Pesada", "Louca", "Adoro frio", "Caço Leões Marinhos" },
        new string[] { "tuba1", "tuba2", "tuba3", "tuba4", "tuba5" },
        new string[] { "urso1", "urso2", "urso3", "urso4", "urso5" },
        new string[] { "ping1", "ping2", "ping3", "ping4", "ping5" },
        new string[] { "agui1", "agui2", "agui3", "agui4", "agui5" },
    };

}
